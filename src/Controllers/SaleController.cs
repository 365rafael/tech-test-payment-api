using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.src.Context;
using tech_test_payment_api.src.Entities;

namespace tech_test_payment_api.src.Controllers
{
    [ApiController]
    [Route("api-docs/[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly PaymentContext _context;

        public SaleController(PaymentContext context)
        {
            _context = context;
        }
        [HttpPost]
        public IActionResult Create(string sellerName, string sellerCpf, string sellerTelephone, string sellerEmail, List<Product> product)
        {
            if (product == null || sellerName == null || sellerCpf == null)
                return BadRequest("Favor inserir os dados da venda corretamente.");


            var sale = new Sale();

            sale.Seller = new Seller(sellerCpf, sellerName, sellerEmail, sellerTelephone);

            sale.Product = product;
            sale.Status = "Aguardando pagamento";
            _context.Add(sale);
            _context.SaveChanges();
            return Ok();
        }

        [HttpGet("{id}")]
        public ActionResult<Sale> SearchSaleId(int id)
        {
            var result = _context.Sales.Where(p => p.Id == id).Include(p => p.Product).Include(s => s.Seller);

            if (result == null)
                return NotFound();

            return Ok(result);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateStatus(int id, string status)
        {
            var saleDB = _context.Sales.Find(id);
            if (saleDB == null)
                return NotFound();

            if (checkStatus(saleDB.Status, status) == false)
                return BadRequest($"A alteração de status desta compra para {status} não é permitida.");

            saleDB.Status = status;
            _context.Sales.Update(saleDB);
            _context.SaveChanges();

            var result = _context.Sales.Where(p => p.Id == id).Include(p => p.Product).Include(s => s.Seller);

            return Ok(result);
        }

        private bool checkStatus(string currentStatus, string newStatus)
        {
            if (currentStatus != "Enviado para transportadora" && newStatus == "Cancelada")
            {
                return true;
            }
            if (currentStatus == "Aguardando pagamento" && newStatus == "Pagamento aprovado")
            {
                return true;
            }
            if (currentStatus == "Pagamento aprovado" && newStatus == "Enviado para transportadora")
            {
                return true;
            }
            if (currentStatus == "Enviado para transportadora" && newStatus == "Entregue")
            {
                return true;
            }

            return false;
        }


    }
}
