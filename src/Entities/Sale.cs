namespace tech_test_payment_api.src.Entities
{
    public class Sale
    {
        public Sale()
        {
            this.Date = DateTime.Now;
            this.Status = "Aguardando Pagamento.";
        }

        public int Id { get; set; }
        public DateTime Date { get; set; }
        public Seller Seller { get; set; }
        public List<Product> Product { get; set; }
        public string Status { get; set; }
    }
}