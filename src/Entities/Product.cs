namespace tech_test_payment_api.src.Entities
{
    public class Product
    {
        public int Id { get; set; }
        public string NameProduct { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int IdSale { get; set; }
    }
}